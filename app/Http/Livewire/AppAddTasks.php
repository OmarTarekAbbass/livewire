<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AppAddTasks extends Component
{
    public $title;
    protected $rules = [
        'title' => 'required|min:4',
    ];

    public function render()
    {
        return view('livewire.app-add-tasks');
    }

    public function updated($validate)
    {
        $this->validateOnly($validate);
    }

    public function addTask()
    {
        $validatedData = $this->validate();
        auth()->user()->tasks()->create([
            'title' => $this->title,
            'status' => false,
        ]);

        $this->title = "";

        $this->emit('taskAdded');
        session()->flash('message','Task Was Added Successfuly !');
    }
}
