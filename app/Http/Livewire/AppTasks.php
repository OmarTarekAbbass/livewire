<?php

namespace App\Http\Livewire;

use App\Models\Task;
use Livewire\Component;
use Livewire\WithPagination;

class AppTasks extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['taskAdded' => '$refresh'];
    public function render()
    {
        $totalTasks = auth()->user()->tasks()->count();
        $tasks = auth()->user()->tasks()->latest()->paginate(5);
        return view('livewire.app-tasks',compact('totalTasks','tasks'));
    }

    public function delete($id)
    {
        if ($id) {
            Task::where('id',$id)->delete();
            session()->flash('message','Task Was Delete Successfuly !');
        }

    }
}
