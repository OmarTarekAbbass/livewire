<div>
<div>
    <h3 class="text-center">Add New Task</h3>

      <p style="color: #38c172;">{{ session('message') }}</p>

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" wire:model="title" class="form-control">
        @error('title') <span class="btn btn-danger" style="margin-top: 2%;">{{ $message }}</span> @enderror
    </div>
    <div class="form-group">
        <button wire:click.prevent="addTask" class="btn btn-primary btn-block">Add</button>
    </div>
    </form>
</div>
</div>
